import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.*;
import static org.openqa.selenium.support.ui.ExpectedConditions.titleIs;


public class checkoutTest {

    @BeforeClass
    public static void setup() {
        Configuration.browser = System.getProperty("driver.browser");
    }

    @Test
    public void testAddToCartAndGoToCheckoutPage() {
        open("https://www.ecobee.com/");
        Selenide.Wait().until(titleIs("Smart WiFi Thermostats by ecobee |"));
        shopOnlineButton.click();
        $("#addToCart").shouldBe(visible).click();
        $(".cart-subtotal").shouldBe(visible);
        continueToShoppingCart.click();
        $(byText("Shopping Cart")).shouldBe(visible);
        continueToShippingMethod.click();
        continueToPaymentMethodButton.should(exist);
    }

    SelenideElement shopOnlineButton = $(".nav--global .no-third-party .ca");
    SelenideElement continueToShoppingCart = $(".to-checkout .btn--primary");
    SelenideElement continueToShippingMethod = $(".normal-cart-footer-subtotal .btn--primary");
    SelenideElement continueToPaymentMethodButton = $(".step__footer__continue-btn");

}